package code;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Player {
	
	static List<Song> songs =new ArrayList<Song>();;
	static Integer index=0;
	
	/**
	 * Shuffles songs list
	 */
	private static void shuffle() {
		System.out.println("Shuffle..\n");
		Random r = new Random();
		int n = songs.size();
		for (int i = n - 1; i > 0; i--) {
			int j = r.nextInt(i + 1);
			Song temp = songs.get(i);
			songs.set(i, songs.get(j));
			songs.set(j, temp);
		}
		index=0;
	}
	
	
	/**
	 * Play all songs until it list is finished
	 */
	private static void playAll() {
		if(index<songs.size()) {
			System.out.println(""+index+"Playing..."+songs.get(index).getName()+"\n");
			++index;
			playAll();
		}else {
			System.out.println("List is finished");
		}
	}
	
	
	/**
	 * List all the songs
	 */
	private static void listAll() {
		for(Song s:songs) {
			System.out.println(s);
		}
		System.out.println();
		
	}

	/**
	 * Initialize songs with dummy data
	 */
	private static void initializeSongs() {		
//		Random rand = new Random();
//		for (int j = 0; j < 100; j++) {
//			int pick=rand.nextInt(100);
//			songs.add(new Song(pick,"Song "+pick));
//		}
		
		songs.add(new Song(1, "Sky full of stars"));
		songs.add(new Song(2, "Yari Yeah"));
		songs.add(new Song(3, "Replaceble"));
		songs.add(new Song(4, "Rooftop party"));
		songs.add(new Song(5, "Pakki gal"));
		songs.add(new Song(6, "Believe in you"));
		songs.add(new Song(7, "Shook"));
		
	}

	/**
	 * Play next song if exists
	 */
	private static void next() {
		if(index<songs.size()-1) {
			++index;
			System.out.println("Next..");
			play();
		}else {
			System.out.println("List is finished");
		}
		
	}

	/**
	 * Play previous song if exists
	 */
	private static void prev() {
		if(index>=1) {
			System.out.println("Prev..");
			--index;
			play();
		}else {
			System.out.println("No Previous Song exists");
		}

	}

	/**
	 * play current index song
	 */
	public static void play() {
		System.out.println(""+index+"Playing..."+songs.get(index).getName()+"\n");
	}
	
	
	/**
	 * Test class
	 * @param args
	 */
	public static void main(String[] args) {
		
		initializeSongs();
		
		listAll();
		
		shuffle();
		
		listAll();
		
		playAll();
		
		prev();
		prev();
		next();
		next();
		
	}
	
	
}
